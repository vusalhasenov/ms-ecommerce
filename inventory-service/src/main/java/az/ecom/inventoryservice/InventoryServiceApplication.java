package az.ecom.inventoryservice;

import az.ecom.inventoryservice.entity.Inventory;
import az.ecom.inventoryservice.repo.InventoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class InventoryServiceApplication implements CommandLineRunner {

    private final InventoryRepository inventoryRepository;

    public static void main(String[] args) {
        SpringApplication.run(InventoryServiceApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        Inventory inventory = new Inventory();
        inventory.setSkuCode("123AZ");
        inventory.setQuantity(100);
        inventoryRepository.save(inventory);
    }
}
