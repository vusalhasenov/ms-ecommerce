package az.ecom.inventoryservice.controller;

import az.ecom.inventoryservice.entity.Inventory;
import az.ecom.inventoryservice.model.InventoryResponse;
import az.ecom.inventoryservice.service.InventoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/inventory")
@RequiredArgsConstructor
public class InventoryController {

    private final InventoryService inventoryService;

    @GetMapping()
    public ResponseEntity<List<InventoryResponse>> isInStock(@RequestParam List<String> skuCode){
        System.out.println(skuCode);
        return ResponseEntity.ok(inventoryService.isInStock(skuCode));
    }

}
