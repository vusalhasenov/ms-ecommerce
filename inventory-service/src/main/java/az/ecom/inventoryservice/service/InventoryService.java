package az.ecom.inventoryservice.service;

import az.ecom.inventoryservice.model.InventoryResponse;

import java.util.List;

public interface InventoryService {

    List<InventoryResponse> isInStock(List<String> skuCode);


}
