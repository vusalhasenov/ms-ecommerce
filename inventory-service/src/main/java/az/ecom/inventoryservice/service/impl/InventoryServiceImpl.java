package az.ecom.inventoryservice.service.impl;

import az.ecom.inventoryservice.model.InventoryResponse;
import az.ecom.inventoryservice.repo.InventoryRepository;
import az.ecom.inventoryservice.service.InventoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class InventoryServiceImpl implements InventoryService {

    private final InventoryRepository repository;

    @Override
    @Transactional(readOnly = true)
    public List<InventoryResponse> isInStock(List<String> skuCode) {
        return repository.findBySkuCodeIn(skuCode)
                .stream().map(inventory ->
                InventoryResponse.builder().
                        skuCode(inventory.getSkuCode())
                        .isInStock(inventory.getQuantity()>0)
                        .build()).collect(Collectors.toList());
    }
}
