package az.ecom.orderservice.mapper;

import az.ecom.orderservice.entity.Order;
import az.ecom.orderservice.entity.OrderLineItems;
import az.ecom.orderservice.model.OrderLineItemsDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface OrderMapper {

    OrderLineItems mapToEntity(OrderLineItemsDto orderLineItemsDto);

    OrderLineItemsDto mapToOrderItemsDto(Order order);

}

