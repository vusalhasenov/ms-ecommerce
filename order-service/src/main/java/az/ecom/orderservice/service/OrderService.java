package az.ecom.orderservice.service;

import az.ecom.orderservice.model.OrderRequestDto;

public interface OrderService {

    void placeOrder(OrderRequestDto orderRequest);
}
