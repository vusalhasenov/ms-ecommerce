package az.ecom.orderservice.service.impl;

import az.ecom.orderservice.entity.Order;
import az.ecom.orderservice.entity.OrderLineItems;
import az.ecom.orderservice.exception.DataNotFoundException;
import az.ecom.orderservice.mapper.OrderMapper;
import az.ecom.orderservice.model.InventoryResponse;
import az.ecom.orderservice.model.OrderRequestDto;
import az.ecom.orderservice.repo.OrderRepository;
import az.ecom.orderservice.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderMapper mapper;
    private final WebClient.Builder client;
    private final OrderRepository repository;

    @Override
    public void placeOrder(OrderRequestDto orderRequest) {
        Order order = new Order();
        order.setOrderNumber(UUID.randomUUID().toString());
        List<OrderLineItems> orderLineItems = orderRequest.getOrderLineItemsDtoList()
                .stream()
                .map(mapper::mapToEntity).collect(Collectors.toList());
        order.setOrderLineItemsList(orderLineItems);

        List<String> skuCode = orderLineItems.stream().map(OrderLineItems::getSkuCode).collect(Collectors.toList());
        InventoryResponse[] inventoryResponsesArray = client.build()
                .get()
                .uri("http://localhost:8081/api/inventory", uriBuilder -> uriBuilder.queryParam("skuCode", skuCode).build())
                .retrieve()
                .bodyToMono(InventoryResponse[].class)
                .block();//
        if (inventoryResponsesArray.length>0) {
            repository.save(order);
        } else {
            throw new DataNotFoundException("product not in stock");
        }
    }
}
