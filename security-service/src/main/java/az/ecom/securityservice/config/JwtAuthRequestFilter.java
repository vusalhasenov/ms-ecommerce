package az.ecom.securityservice.config;

import az.ecom.securityservice.service.JwtService;
import com.google.common.net.HttpHeaders;
import io.jsonwebtoken.Claims;
import io.micrometer.common.util.StringUtils;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
public class JwtAuthRequestFilter extends OncePerRequestFilter {
    public static final String BEARER = "Bearer";
    public static final String AUTHORITIES_CLAIM = "authorities";
    private final JwtService jwtService;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                                    FilterChain filterChain) throws IOException, ServletException {
        final Optional<Authentication> authentication = getAuthentication(
                extractJwt(httpServletRequest.getHeader(HttpHeaders.AUTHORIZATION)));
        System.out.println(authentication);
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

    public Optional<String> extractJwt(String authorizationHeader) {
        if (StringUtils.isBlank(authorizationHeader) ||
                !authorizationHeader.startsWith(BEARER)) {
            return Optional.empty();
        }

        final String jwt = authorizationHeader.substring(BEARER.length())
                .trim();
        return Optional.of(jwt);
    }

    private Optional<Authentication> getAuthentication(Optional<String> jwt) {
        if (jwt.isEmpty()) {
            return Optional.empty();
        }
        try {
            final Claims claims = jwtService.parseToken(jwt.get());
            final UsernamePasswordAuthenticationToken authenticationToken =
                    new UsernamePasswordAuthenticationToken(claims.getSubject(),
                            "", getUserAuthorities(claims));
            return Optional.of(authenticationToken);
        } catch (RuntimeException exception) {
            log.trace("Jwt parsing failed ", exception);
        }
        System.out.println("Jwt is expired");
        return Optional.empty();
    }

    private Collection<? extends GrantedAuthority> getUserAuthorities(Claims claims) {
        List<?> roles = claims.get(AUTHORITIES_CLAIM, List.class);
        return roles
                .stream()
                .map(a -> new SimpleGrantedAuthority(a.toString()))
                .collect(Collectors.toList());
    }

}
